import numpy
import pandas
from tensorflow import keras
from tensorflow.keras import regularizers, initializers, layers, optimizers


class Model(keras.Sequential):
	def __init__(self, *args, n_classes=5, **kw):
		super().__init__(*args, **kw)
		self.add(layers.Conv1D(kernel_size = 5, strides=1,filters = 32, activation='relu',input_shape=(50,1)))
		self.add(layers.MaxPooling1D(pool_size = (2), strides=(2)))
		self.add(layers.Conv1D(kernel_size = 5, strides=1, filters = 64, activation='relu'))
		self.add(layers.MaxPooling1D(pool_size = (2), strides=(2)))
		self.add(layers.Flatten())
		self.add(layers.Dense(1000, activation='relu'))
		self.add(layers.Dense(n_classes, activation = 'softmax'))
		self.compile(loss='categorical_crossentropy', optimizer=optimizers.Adam(lr=0.001),metrics=['accuracy'])




