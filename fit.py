from model import Model
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, StandardScaler
from tensorflow import keras
# from tensorflow.keras.models import load_model
from pathlib import Path


encoder = LabelEncoder()
encoder.fit(labels)
encoded_Y = encoder.transform(labels)
dummy_y = keras.utils.to_categorical(encoded_Y,num_classes=5)
df = pd.read_csv("final.csv")
labels = df.iloc[:,-1]
data = df.drop(df.columns[50],axis=1)
X_train, X_test, y_train, y_test = train_test_split(data, dummy_y, test_size=0.05, random_state=42,shuffle = True)
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
X1 = np.expand_dims(X_train, axis=2)
model = Model()
model.fit(X1, y_train, epochs=10, batch_size=100)
model.save('./model.save')


